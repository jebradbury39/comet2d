## Comet2D

This library is written in Jasmint with the goal of being transpiled to C#
and C++. This will allow it to run in Unity on the client side, and in C++ on
the server side, and all calculations should match as closely as is reasonable.
