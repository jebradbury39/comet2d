ffi module arbiter of comet2d;

import jsmnt_global.CmpResult;

import std.math;

import comet2d.body.RigidBody;
import comet2d.collide;
import comet2d.util;
import comet2d.util.Vec2;
import comet2d.World;

export enum EdgeNumber {
   none,
   edge1,
   edge2,
   edge3,
   edge4
}

export class Edges {
   public {
      EdgeNumber inEdge1 = EdgeNumber.none;
      EdgeNumber outEdge1 = EdgeNumber.none;
      EdgeNumber inEdge2 = EdgeNumber.none;
      EdgeNumber outEdge2 = EdgeNumber.none;

      init() {
      }
   }
}

export class FeaturePair {
   public {
      Edges e = Edges();
      int value = 0;

      init() {
      }

      fun void update() {
         value = cast<int>(e.inEdge1) + cast<int>(e.outEdge1) + cast<int>(e.inEdge2) + cast<int>(e.outEdge2);
      }

      fun void flip() {
         EdgeNumber tmp = e.inEdge1;
         e.inEdge1 = e.inEdge2;
         e.inEdge2 = tmp;

         tmp = e.outEdge1;
         e.outEdge1 = e.outEdge2;
         e.outEdge2 = tmp;

      }
   }
}

export ffi class Contact {
   public {
      Vec2 position = Vec2();
      Vec2 normal = Vec2();
      Vec2 r1 = Vec2();
      Vec2 r2 = Vec2();
      float32 separation = 0;
      float32 Pn = 0; // accumulated normal impulse
      float32 Pt = 0; // accumulated tangent impulse
      float32 Pnb = 0; // accumulated normal impulse for position bias
      float32 massNormal = 0;
      float32 massTangent = 0;
      float32 bias = 0;
      FeaturePair feature = FeaturePair();
   
      init() {
      }
   }
}

export class ArbiterKey {
   public {
      RigidBody* body1;
      RigidBody* body2;

      init(RigidBody* b1, RigidBody* b2) {
         if (b1.uid < b2.uid) {
            body1 = b1;
            body2 = b2;
         } else {
            body1 = b2;
            body2 = b1;
         }
      }

      override fun int hash() {
         return cast<int>(body1.uid + body2.uid);
      }

      override fun CmpResult cmp(Obj* other) {
         if (other !instanceof ArbiterKey) {
            return CmpResult.NE;
         }
         ArbiterKey* otherArb = cast<ArbiterKey*>(other);
         if (body1.uid > otherArb.body1.uid) {
            return CmpResult.LT;
         }
         if (body1.uid < otherArb.body1.uid) {
            return CmpResult.GT;
         }
         
         if (body2.uid > otherArb.body2.uid) {
            return CmpResult.LT;
         }
         if (body2.uid < otherArb.body2.uid) {
            return CmpResult.GT;
         }

         return CmpResult.EQ;
      }
   }
}

export ffi class Arbiter {
   public {
      static const int32 max_points = 2;

      RigidBody* body1;
      RigidBody* body2;
      [Contact] contacts = [Contact][];
      float32 combinedFriction = 0;

      init(RigidBody* b1, RigidBody* b2) {
         if (b1.uid < b2.uid) {
            body1 = b1;
            body2 = b2;
         } else {
            body1 = b2;
            body2 = b1;
         }

         collide.computeCollide(&contacts, body1, body2);
         combinedFriction = math.sqrtf(body1.friction * body2.friction);
      }

      fun void update([Contact]* newContacts) {
         [Contact] mergedContacts = [Contact][];

         for (int i = 0; i < newContacts.size(); i += 1) {
            Contact* cNew = &(newContacts[i]);
            int k = -1;
            for (int j = 0; j < contacts.size(); j += 1) {
               Contact* cOld = &(contacts[j]);
               if (cNew.feature.value == cOld.feature.value) {
                  k = j;
                  break;
               }
            }

            if (k > -1) {
               mergedContacts.add(*cNew);
               Contact* cOld = &(contacts[k]);
               Contact* c = &(mergedContacts[i]);
               if (World.warmStarting) {
                  c.Pn = cOld.Pn;
                  c.Pt = cOld.Pt;
                  c.Pnb = cOld.Pnb;
               } else {
                  c.Pn = 0;
                  c.Pt = 0;
                  c.Pnb = 0;
               }
            } else {
               mergedContacts.add(newContacts[i]);
            }
         }

         int contactsSize = cast<int>(contacts.size());
         for (int i = 0; i < newContacts.size(); i += 1) {
            if (contactsSize > i) {
               contacts[i] = newContacts[i];
            } else {
               contacts.add(newContacts[i]);
               contactsSize += 1;
            }
         }
      }

      fun void preStep(float32 inverseDeltaTime) {
         float32 k_allowedPenetration = 0.01;
         float32 k_biasFactor = 0;
         if (World.positionCorrection) {
            k_biasFactor = 0.2;
         }

         for (int i = 0; i < contacts.size(); i += 1) {
            Contact* c = &(contacts[i]);
            Vec2 r1 = c.position.sub(&body1.position);
            Vec2 r2 = c.position.sub(&body2.position);

            float32 dotR1 = util.dot(&r1, &r1);
            float32 dotR2 = util.dot(&r2, &r2);

            // precompute the normal mass, tangent mass, and bias
            float32 rn1 = util.dot(&r1, &c.normal);
            float32 rn2 = util.dot(&r2, &c.normal);
            float32 kNormal = body1.invMass + body2.invMass;
            kNormal += body1.invInertia * (dotR1 - (rn1 * rn1)) + body2.invInertia * (dotR2 - (rn2 * rn2));
            c.massNormal = 1.0 / kNormal;

            Vec2 tangent = util.cross(&c.normal, 1.0);
            float rt1 = util.dot(&r1, &tangent);
            float rt2 = util.dot(&r2, &tangent);
            float kTangent = body1.invMass + body2.invMass;
            kTangent += body1.invInertia * (dotR1 - (rt1 * rt1)) + body2.invInertia * (dotR2 - (rt2 * rt2));
            c.massTangent = 1.0 / kTangent;

            c.bias = -1.0 * k_biasFactor * inverseDeltaTime * util.min(0.0, c.separation + k_allowedPenetration);

            if (World.accumulateImpulses) {
               // apply normal + friction impulse
               Vec2 tmp = tangent.mul(c.Pt);
               Vec2 P = c.normal.mul(c.Pn).add(&tmp);
               
               tmp = P.mul(body1.invMass);
               body1.velocity = body1.velocity.sub(&tmp);
               body1.angularVelocity -= body1.invInertia * util.cross(&r1, &P);

               tmp = P.mul(body2.invMass);
               body2.velocity = body2.velocity.add(&tmp);
               body2.angularVelocity += body2.invInertia * util.cross(&r2, &P);
            }
         }
      }

      fun void applyImpulse() {
         RigidBody* b1 = body1;
         RigidBody* b2 = body2;

         for (int i = 0; i < contacts.size(); i += 1) {
            Contact* c = &(contacts[i]);
            c.r1 = c.position.sub(&b1.position);
            c.r2 = c.position.sub(&b2.position);

            // relative velocity at contact
            Vec2 tmp = util.cross(b2.angularVelocity, &c.r2);
            Vec2 dv = b2.velocity.add(&tmp);
            dv = dv.sub(&b1.velocity);
            tmp = util.cross(b1.angularVelocity, &c.r1);
            dv = dv.sub(&tmp);

            // compute normal impulse
            float32 vn = util.dot(&dv, &c.normal);

            float32 dPn = c.massNormal * (-1.0 * vn + c.bias);

            if (World.accumulateImpulses) {
               // clamp the accumulated impulse
               float32 Pn0 = c.Pn;
               c.Pn = util.max(Pn0 + dPn, 0.0);
               dPn = c.Pn - Pn0;
            } else {
               dPn = util.max(dPn, 0.0);
            }

            // apply contact impulse
            Vec2 Pn = c.normal.mul(dPn);
            
            Vec2 tmp = Pn.mul(b1.invMass);
            b1.velocity = b1.velocity.sub(&tmp);
            b1.angularVelocity -= b1.invInertia * util.cross(&c.r1, &Pn);

            tmp = Pn.mul(b2.invMass);
            b2.velocity = b2.velocity.add(&tmp);
            b2.angularVelocity += b2.invInertia * util.cross(&c.r2, &Pn);

            // update relative velocity at contact
            tmp = util.cross(b2.angularVelocity, &c.r2);
            dv = b2.velocity.add(&tmp);
            dv = dv.sub(&b1.velocity);
            tmp = util.cross(b1.angularVelocity, &c.r1);
            dv = dv.sub(&tmp);

            Vec2 tangent = util.cross(&c.normal, 1.0);
            float32 vt = util.dot(&dv, &tangent);
            float32 dPt = c.massTangent * -1.0 * vt;

            if (World.accumulateImpulses) {
               // compute friction impulse
               float32 maxPt = combinedFriction * c.Pn;

               // clamp friction
               float32 oldTangentImpulse = c.Pt;
               c.Pt = util.clamp(oldTangentImpulse + dPt, -maxPt, maxPt);
               dPt = c.Pt - oldTangentImpulse;
            } else {
               float32 maxPt = combinedFriction * dPn;
               dPt = util.clamp(dPt, -maxPt, maxPt);
            }

            // apply contact impulse
            Vec2 Pt = tangent.mul(dPt);
            
            Vec2 tmp = Pt.mul(b1.invMass);
            b1.velocity = b1.velocity.sub(&tmp);
            b1.angularVelocity -= b1.invInertia * util.cross(&c.r1, &Pt);

            tmp = Pt.mul(b2.invMass);
            b2.velocity = b2.velocity.add(&tmp);
            b2.angularVelocity += b2.invInertia * util.cross(&c.r2, &Pt);
         }
      }
   }
}
